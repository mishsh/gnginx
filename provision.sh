#!/usr/bin/env bash 
##############################################
#name: michaell-vaknin
#porpuse:Deploy flask app with Nginx using Gunicorn
#date:13.03.21
#version:v1.0.15
#############################################

_pkgs=(python3-pip python3-dev )

main(){
	all_install	
	clone_git
	virtual_env
	systemd_conf
	conf_nginx
}



all_install(){

	for i in ${_pkgs[@]}
		do
		sudo apt update -y 
		sudo apt install $_pkgs -y
		sudo pip3 install virtualenv
	done
}

clone_git(){

	git clone https://gitlab.com/mishsh/gnginx 
	sudo chown -R vagrant:vagrant /home/vagrant/gnginx/src
}


virtual_env(){

	sudo virtualenv /home/vagrant/gnginx/src/myprojectvenv
	sudo pip3 install -r /home/vagrant/gnginx/src/requirements.txt
}

systemd_conf(){
	sudo echo -n "
[Unit]
Description=Flask Daemon
After=network.target

[Service]
User=vagrant
Group=www-data
WorkingDirectory=/home/vagrant/gnginx/src
Environment="PATH=/home/vagrant/gnginx/src/myprojectvenv/bin"
ExecStart=/usr/local/bin/gunicorn --workers 3 --bind unix:app.sock -m 007 wsgi:app

[Install]
WantedBy=multi-user.target
	" > /etc/systemd/system/app.service
	sudo systemctl start app
	sudo systemctl enable app
}

conf_nginx(){
	echo -n '
server {
	listen 80;
	server_name 10.0.2.15;

	location / {
		include proxy_params;
		proxy_pass http://unix:/home/vagrant/gnginx/src/app.sock;
	}
}' > /etc/nginx/sites-available/app
if [[ $(nginx -t > /dev/null; echo $?) == 0 ]];then
	sudo ln -s /etc/nginx/sites-available/app /etc/nginx/sites-enabled
	sudo systemctl restart  nginx
	sudo ufw allow 'Nginx Full'
else
	echo "there is an error with nginx conf"
fi
}





main
